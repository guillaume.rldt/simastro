package main;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;


public class Systeme{
	double g;//gravité
	double dt;//pas de temps pour la simulation
	int fa;//facteur d'accéleration
	int rayon;//rayon du système
	Vaisseau vaisseau;
	Soleil soleil;
	ArrayList<Planete> planetes = new ArrayList<Planete>();
	
	public Systeme() {
		
	}
	
	public Systeme(Systeme s) {
		this.planetes = s.planetes;
		this.vaisseau = s.getVaisseau();
		this.g = s.getG();
		this.dt = s.getDt();
		this.fa = s.getFa();
		this.rayon = s.getRayon();
	}
	
	public double getG() {
		return g;
	}
	public double getDt() {
		return dt;
	}
	public int getFa() {
		return fa;
	}
	public int getRayon() {
		return rayon;
	}
	public Vaisseau getVaisseau() {
		return vaisseau;
	}
	public ArrayList<Planete> getPlanetes() {
		return planetes;
	}
	
}
