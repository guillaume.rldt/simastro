package main;

import javafx.scene.image.Image;

public class Vaisseau extends Simule{
	double pprincipal;
	double pretro;
	final int TAILLE = 50;
	final Image img = new Image("file:"+"res/terre.png");
	
	public Vaisseau(int x, int y, double vx, double vy, double m, double pp, double pr) {
		super(x, y, vx, vy, m);
		pprincipal = pp;
		pretro = pr;
	}	 

}
