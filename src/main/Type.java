package main;

public enum Type {
	Fixe('F'), Simule('S'), Cercle('C'), Elipse('E');
	
	private char type;
	
	Type(char c) {
		type=c;
	}

}
