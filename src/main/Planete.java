package main;

import javafx.scene.image.Image;

public class Planete {
	String nom;
	Type type;
	double masse;
	double posx;
	double posy;
	double vitx;
	double vity;
	private double taille;
	private Image img;

	
	public Planete(String nom, Type type, double masse, double posx, double posy, double vitx, double vity, double taille) {
		this(nom,type,masse,posx,posy,vitx,vity,taille,new Image("file:"+"res/terre.png"));
	}
	
	public Planete(String nom, Type type, double masse, double posx, double posy, double vitx, double vity, double taille, Image img) {
		this.nom = nom;
		this.type = type;
		this.masse = masse;
		this.posx = posx;
		this.posy = posy;
		this.vitx = vitx;
		this.vity = vity;
		this.taille = taille;
		this.img = img;
	}
	
	public Planete(String nom, Type type, double masse, double posx, double posy, double taille) {
		this(nom,type,masse,posx,posy,taille,new Image("file:"+"res/terre.png"));
		
	}
	
	public Planete(String nom, Type type, double masse, double posx, double posy, double taille, Image img) {
		this.nom = nom;
		this.type = type;
		this.masse = masse;
		this.posx = posx;
		this.posy = posy;
		this.vitx = 0;
		this.vity = 0;
		this.taille = taille;
	}

	public double getPosx() {
		return posx;
	}

	public void setPosx(int posx) {
		this.posx = posx;
	}

	public double getPosy() {
		return posy;
	}

	public void setPosy(int posy) {
		this.posy = posy;
	}

	public double getTaille() {
		return taille;
	}

	public Image getImg() {
		return img;
	}
}
