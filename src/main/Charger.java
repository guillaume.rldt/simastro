package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;

public class Charger {
	
	/*static void sauve(Systeme systeme) {
		try {
			@SuppressWarnings("resource")
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("res/systeme.txt")));
			oos.writeObject(systeme);
		} 
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	static Systeme charge() {
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("res/systeme.txt")))) {
			Systeme tmp = (Systeme) ois.readObject();
			return tmp;
		} 
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}*/
	
	public static Systeme charge() throws IOException {
		File file = new File("res/systeme.txt");
		Systeme sys = new Systeme();
		
		List<Double> valeurs = new ArrayList<>();
		int idxval = 0;
		
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line = buffer.readLine();
		String [] valtmp = null;
		while (line != null) {
			valtmp = line.split(" ");
			if (valtmp[1].equals("Fixe")) {
				sys.soleil = parseLigneSoleil(valtmp);
			}else if (valtmp[1].equals("Simule")) {
				sys.planetes.add(parseLignePlanete(valtmp));
			}else if (valtmp[1].equals("Vaisseau")) {
				sys.vaisseau = parseLigneVaisseau(valtmp);
			}else {
				//non reconnu
			}
		}
		
		return sys;
	}
	
	public static void main(String[] args) throws IOException {		
		Systeme sys = new Systeme(charge());//boucle infinie
		
		System.out.println(sys.toString());
		
		
	}
	
	public static double returnVal(String s) {
		double res = -666;//code erreur
		String resString = "";
		boolean egalPresent = false;
		
		for(int i=0; i<s.length(); i++) {
			if(egalPresent) {
				resString += s.charAt(i);
			}
			if(s.charAt(i) == '=') {
				egalPresent = true;
			}
		}
		if(!resString.isEmpty()) {
			res = Double.valueOf(resString);
		}
		return res;
	}
	
	public static Soleil parseLigneSoleil(String[] s) {
		int posx, posy;
		double masse;
	
		masse = Double.valueOf(s[2]);
		posx = Integer.valueOf(s[3]);
		posy = Integer.valueOf(s[4]);
		
		return new Soleil(masse,posx,posy);
	}
	
	public static Planete parseLignePlanete(String[] s) {
		String nom = s[0];
		Type type = Type.Simule;
		double masse = Double.valueOf(s[2]);
		int posx = Integer.valueOf(s[3]);
		int posy = Integer.valueOf(s[4]);
		double vitx = Double.valueOf(s[5]);
		double vity = Double.valueOf(s[6]);
		double taille = 50;//par defaut
	
		return new Planete(nom,type,masse,posx,posy,vitx,vity,taille);
	}
	
	public static Vaisseau parseLigneVaisseau(String[] s) {
		//String nom = s[0];		
		double masse = Double.valueOf(s[2]);
		int posx = Integer.valueOf(s[3]);
		int posy = Integer.valueOf(s[4]);
		double vitx = Double.valueOf(s[5]);
		double vity = Double.valueOf(s[6]);
		double pprincipal = Double.valueOf(s[7]);
		double pretro = Double.valueOf(s[8]);
		
		return new Vaisseau(posx,posy,vitx,vity,masse,pprincipal,pretro);
	}
	
	
}
