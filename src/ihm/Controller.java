package ihm;

import java.awt.Point;
import java.io.IOException;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import main.Charger;
import main.Planete;
import main.Systeme;

public class Controller {

	@FXML
	private Canvas canvas;

	@FXML
	private Button rentrer;

	//model model
	private GraphicsContext graphiqueContext;
	
	Systeme systeme;
   
    public void register(/*Observer observer*/) {
    	//model.addObserver(observer);
    }
    
    public void affichage(GraphicsContext nouveau) {
    	graphiqueContext = nouveau;
    }

	public GraphicsContext getGraphiqueContext() {
		return graphiqueContext;
	}

	public void setGraphiqueContext(GraphicsContext graphiqueContext) {
		this.graphiqueContext = graphiqueContext;
	}

	@FXML
	private Menu aide;

	@FXML
	private MenuItem close;

	@FXML
	private MenuItem charger;

	@FXML
	void close(ActionEvent event) {
		Stage s = (Stage) canvas.getScene().getWindow();
		s.close();
	}

	@FXML
	void aide(ActionEvent event) {

	}

	@FXML
	void charger(ActionEvent event) {
		try {
			systeme = new Systeme(Charger.charge());
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		affichage(convertion(systeme.getPlanetes()));
	}

	public void initialize() {
		graphiqueContext = canvas.getGraphicsContext2D();
		graphiqueContext.fillRect(0, 0, 1600, 900);
		affichage(convertion());
	}
	
	
	//avec une image
	/*
	public GraphicsContext convertion(List<Planete> objetCeleste) {
		GraphicsContext res = new Canvas().getGraphicsContext2D();

		for (int i = 0 ; i < objetCeleste.size(); i++) {
			Point posObjet = new Point(objetCeleste.get(i).getPosx(),objetCeleste.get(i).getPosy());
			Image img = objetCeleste.get(i).getImg();
			double taille = objetCeleste.get(i).getTaille()/canvas.getWidth();
			graphiqueContext.drawImage(img, posObjet.getX()/canvas.getWidth() + canvas.getWidth()/2 - taille/2, posObjet.getY()/canvas.getHeight() + canvas.getHeight()/2 - taille/2, taille, taille);
		}
		return res;
	}*/
   
	//sans image
	public GraphicsContext convertion(List<Planete> objetCeleste) {
		GraphicsContext res = new Canvas().getGraphicsContext2D();

		for (int i = 0 ; i < objetCeleste.size(); i++) {
			Point posObjet = new Point(objetCeleste.get(i).getPosx(),objetCeleste.get(i).getPosy());
			double taille = objetCeleste.get(i).getTaille()/canvas.getWidth();
			graphiqueContext.setFill(Color.WHITE);
			graphiqueContext.fillOval(posObjet.getX()/canvas.getWidth() - taille/2, posObjet.getY()/canvas.getHeight() - taille/2, taille, taille);
		}
		return res;
	}

	public GraphicsContext convertion() {
		GraphicsContext res = new Canvas().getGraphicsContext2D();

		Point soleil = new Point(0,0);
		double taille = 50;
		graphiqueContext.setFill(Color.WHITE);
		graphiqueContext.fillOval(soleil.getX()/canvas.getWidth() + canvas.getWidth()/2 - taille/2, soleil.getY()/canvas.getHeight() + canvas.getHeight()/2 - taille/2, taille, taille);
		return res;
	}
}

